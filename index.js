"use strict";

exports.serviceResponse = (req, res) => {
  const { choice, output, speechToTextHints, responseOverwrites, dynamicEntityOverwrites } = req.body.input;
  
  const parsedSpeechToTextHints = JSON.parse(speechToTextHints || "[]");
  
  console.log(`Sending sttHints of length ${parsedSpeechToTextHints.length}: ${JSON.stringify(parsedSpeechToTextHints, null, 2)}`)
  
  res.send({
    choice: choice || "continue",
    output,
    speechToTextHints: parsedSpeechToTextHints,
    responseOverwrites: JSON.parse(responseOverwrites || "{}"),
    dynamicEntityOverwrites: JSON.parse(dynamicEntityOverwrites || "{}")
  });
};
